""" Module for Trace model determination

This module is containing only one class 'Trace'
  * Trace - Trace model description class
"""

__author__ = 'Denis V. Gruzenkin'
__name__ = 'Trace (Point instances set) in N-dimensional space'
__date__ = '09.11.2016'

from models.vector import Vector


class Trace:
    """ Trace (Point instances set) in N-dimensional space class

    Properties:
        * name - Class instance's name
        * points_set - Tuple of Point class instances

    Methods:
        * __init__(self, new_name, new_points_set) - Constructor
        * __eq__(self, other) - Overload operator '==' for Trace type objects
        * __ne__(self, other) - Overload operator '!=' for Trace type objects
        * is_eq - Method, which detect mathematical equivalent of traces
    """

    def __init__(self, new_name='noSetName', new_points_set=()):
        """ Class constructor

        :type new_name: str
        :param new_name: set name, by default is 'noSetName'
        :type new_points_set: tuple
        :param new_points_set: set of Point instances, i.e. a trace in N-dimensional space, by default is empty tuple
        :return: new Trace instance
        """

        self.name = new_name
        self.points_set = new_points_set
        self.forward_way = Vector()
        if len(self.points_set) > 0:
            self.forward_way.point_start = self.points_set[0]
            self.forward_way.point_end = self.points_set[len(self.points_set) - 1]
        self.edges = []
        for i in range(0, len(self.points_set) - 1):
            vector_tmp = Vector(self.points_set[i], self.points_set[i+1])
            self.edges.append(vector_tmp)

    def get_forward_way_length(self):
        """ Calculate a length of way between trace's head and tail

        :return: Number value of forward way's length
        """

        return self.forward_way.get_length()

    def get_trace_length(self):
        """ Calculate all trace length

        :return: Number value of all trace length
        """

        result = 0
        for edge in self.edges:
            result += edge.get_length()

        return result

    def __eq__(self, other):
        """ Equivalent operation overload (Traces's programs are clones)

        :type other: Trace
        :param other: the 2-nd operand
        :return: True - if all points of both sets are equivalent; False - otherwise
        """

        result = True
        if len(self.points_set) == len(other.points_set):
            for i in range(0, len(self.points_set)):
                if self.points_set[i] != other.points_set[i]:
                    result = False
                    break
        else:
            result = False

        return result

    def __ne__(self, other):
        """ Not equivalent operation overload (Traces's programs are not clones)

        :type other: Trace
        :param other: the 2-nd operand
        :return:  True - if traces are not equivalent; False - otherwise
        """

        result = False
        if len(self.points_set) == len(other.points_set):
            for i in range(0, len(self.points_set)):
                if self.points_set[i] != other.points_set[i]:
                    result = True
                    break
        else:
            result = True

        return result

    def is_eq(self, other):
        """ 2 traces is equivalent, if the 1-st and the last points of them are equivalent

        i.e. if input parameters and result value are similar
        :type other: Trace
        :param other: Other Trace instance, which is comparing with the self argument
        :return: True - if the 1-st and the last points are equivalent; False - otherwise
        """

        if self.points_set[0] == other.points_set[0] and other.points_set[
                    len(other.points_set) - 1] == self.points_set[len(self.points_set) - 1]:
            return True
        else:
            return False

    def ratio_dimension_length(self):
        """ Function determines a ration of space dimensions to trace length

            * If a ration of space dimensions to trace length is more then 1, then an algorithm completes more then 1
            calculation per 1 step.
            * If it is equal to 1, then an algorithm completes only 1 calculation per 1 step.
            * If it is equal to 1, then an algorithm completes less then 1 calculation per 1 step.
        :return: float number, which means a ration of space dimensions to trace length
        """

        return len(self.points_set[0].position) / len(self.points_set)