""" Module for Vector model determination

This module is containing only one class 'Vector'
  * Vector - Vector model description class
"""

# TODO: Написать класс вектора, тесты к нему, добавить кортеж векторов в трассу,добавить метод пересчёта векторов при инициализации/изменении точек!!!

__author__ = 'Denis V. Gruzenkin'
__name__ = 'Trace (Point instances set) in N-dimensional space'
__date__ = '20.02.2017'

from .point import Point
from math import sqrt, acos, pi


class Vector:
    """ Vector, which consists from 2 Points

    Properties:
        * point_start - head of vector
        * point_end - tail of vector
    """

    def __init__(self, pnt1=Point(), pnt2=Point()):
        """ Class constructor

        :type pnt1: Point
        :param pnt1: vector's the 1-st point
        :type pnt2: Point
        :param pnt2: vector's the end point
        :return: Vector instance
        """

        self.point_start = pnt1
        self.point_end = pnt2

    def get_scalar_value(self):
        """ Calculate a scalar product of 2 points as vector

        :return: Tuple of number values
        """

        return (self.point_start * self.point_end).position

    def get_scalar_product(self):
        """ Calculate a scalar product of vector's 2 points

        :return: Number value of scalar product
        """
        # В данном случае под вектором понимается точка в N-мерном пространстве, это необходимо для нахождения
        # расстояния между точками
        scalar_value = self.get_scalar_value()
        result = 0
        for item in scalar_value:
            result += item

        return result

    def get_length(self):
        """ Calculate a length between start and end points

        :return: Number value of length between start and end points
        """
        # Длина определяется как квадратный корень из скалярного произведения векторов
        return sqrt(abs(self.get_scalar_product()))

    def get_corner_between_pnts(self):
        """ Calculate a corner between points

        Подробнее о нахождении угла https://ru.wikipedia.org/wiki/Скалярное_произведение
        :return: Corner between points in radians
        """
        #        |       Скалярное произведение делим на произведение мощьностей весторов = cos угла       |
        my_cos = self.get_scalar_product() / (len(self.point_start.position) * len(self.point_end.position))
        result = acos((my_cos * pi) / 180)  # Получаем значение угла в радианах, применяя функцию arccos
        return result

    def get_corner_between_vectors(self, other):
        """ Calculate a corner between vectors

        :type other: Vector
        :param other: The 2-nd operand
        :return: Corner between vectors in radians or False, if lengths of 2 vectors are different
        """

        vctr1 = self.get_scalar_value()
        vctr2 = other.get_scalar_value()
        # Длины векторов должны быть одинаковы, т.к. мерность пространства остаётся неизменной
        if len(vctr1) == len(vctr2):
            scalar_product = 0  # Если скалярное произведение векторов равно 0, то они ортагональны
            for i in range(0, len(vctr1)):
                scalar_product += vctr1[i] * vctr2[i]
            return acos((scalar_product / (len(vctr1) * len(vctr2))) * pi / 180)
        else:
            return False

    def __eq__(self, other):
        """ Equivalent operation overload

        :type other: Vector
        :param other: The 2-nd operand
        :return: True, if 2 vectors are equivalent, False - otherwise
        """

        if self.point_start == other.point_start and self.point_end == other.point_end:
            return True
        else:
            return False

    def __ne__(self, other):
        """ Not equivalent operation overload

        :type other: Vector
        :param other: The 2-nd operand
        :return: True, if 2 vectors are not equivalent, False - otherwise
        """

        if self.point_start != other.point_start or self.point_end != other.point_end:
            return True
        else:
            return False
