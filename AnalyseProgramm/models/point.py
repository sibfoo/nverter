""" Module for Point model determination

This module is containing only one class 'Point' and one variable 'ExceptionMsg'
  * ExceptionMsg is containing exception error message. It is located outside from class, since it is not necessary to
    create such property for every Point class instance.
      So module connection should looks like: 'from models.point import Point'
  * Point - Point model description class
"""

__author__ = 'Denis V. Gruzenkin'
__name__ = 'Point of N-dimensional space'
__date__ = '07.11.2016'

ExceptionMsg = 'Tuples are not match!'  # Error message string


class Point:
    """ Point of N-dimensional space class

    Properties:
        * name - point name
        * position - tuple, which containing point coordinates
    """

    def __init__(self, new_name='noName', data=()):
        """ Class constructor

        :type new_name: str
        :param new_name: name of new instance, by default is 'noName'
        :type data: tuple
        :param data: coordinates of new instance, by default is empty tuple
        :return: new Point instance
        """
        self.position = data
        self.name = new_name

    def __str__(self):
        return self.name + str(self.position)

    def __add__(self, other):  # Сложение
        """ Adding operation overload

        :type other: Point
        :param other: Other Point instance, which is the 2-nd operand of operation
        :return: Point instance
        """
        # self.mathAction(other, '+')
        if len(self.position) == len(other.position):
            lst = []
            for i in range(0, len(self.position)):  # TODO: Сделать нормальную реализацию через eval!!!
                lst.append(self.position[i] + other.position[i])
            return Point(self.name + '+' + other.name, tuple(lst))
        else:
            raise NameError(ExceptionMsg)

    def __sub__(self, other):  # Вычитание
        """ Subtraction operation overload

        :type other: Point
        :param other: Other Point instance, which is the 2-nd operand of operation
        :return: Point instance
        """
        # self.mathAction(other, '-')
        if len(self.position) == len(other.position):
            lst = []
            for i in range(0, len(self.position)):  # TODO: Сделать нормальную реализацию через eval!!!
                lst.append(self.position[i] - other.position[i])
            return Point(self.name + '-' + other.name, tuple(lst))
        else:
            raise NameError(ExceptionMsg)

    def __mul__(self, other):  # Умножение
        """ Multiplication operation overload

        :type other: Point
        :param other: Other Point instance, which is the 2-nd operand of operation
        :return: Point instance
        """
        # self.mathAction(other, '*')
        if len(self.position) == len(other.position):
            lst = []
            for i in range(0, len(self.position)):  # TODO: Сделать нормальную реализацию через eval!!!
                lst.append(self.position[i] * other.position[i])
            return Point(self.name + '*' + other.name, tuple(lst))
        else:
            raise NameError(ExceptionMsg)

    def __truediv__(self, other):  # Деление
        """ Dividing operation overload

        :type other: Point
        :param other: Other Point instance, which is the 2-nd operand of operation
        :return: Point instance
        """
        # self.mathAction(other, '/')
        if len(self.position) == len(other.position):
            lst = []
            for i in range(0, len(self.position)):  # TODO: Сделать нормальную реализацию через eval!!!
                lst.append(self.position[i] / other.position[i])
            return Point(self.name + '/' + other.name, tuple(lst))
        else:
            raise NameError(ExceptionMsg)

    def __eq__(self, other):  # Сравнение - равны
        """ Equivalent operation overload

        :type other: Point
        :param other: Other Point instance, which is the 2-nd operand of operation
        :return: True - if points are equivalent; False - otherwise
        """

        if self.position == other.position:
            result = True
        else:
            result = False

        return result

    def __ne__(self, other):  # Сравнение - не равны
        """ Not equivalent operation overload

        :type other: Point
        :param other: Other Point instance, which is the 2-nd operand of operation
        :return: True - if points are not equivalent; False - otherwise
        """

        if self.position != other.position:
            result = True
        else:
            result = False

        return result

    # def mathAction(self, other, operation):
    #     if len(self.position) == len(other.position):
    #         lst = []
    #         for i in range(0, len(self.position)): # TODO: Сделать нормальную реализацию через eval!!!
    #             if operation == '+':
    #                 lst.append(self.position[i] + other.position[i])
    #             elif operation == '-':
    #                 lst.append(self.position[i] - other.position[i])
    #             elif operation == '*':
    #                 lst.append(self.position[i] * other.position[i])
    #             elif operation == '/':
    #                 lst.append(self.position[i] / other.position[i])
    #         return tuple(lst)
    #     else:
    #         raise NameError(ExceptionMsg)
