""" Analyse library for Trace objects

This library contains several simple functions for objects of Trace class analysis
Methods:
    is_clones(trace1, trace2) - Function for Trace clones detecting
    is_equal(trace1, trace2) - Function for equal Traces detecting
    get_matched_points(trace1, trace2) - Function returns tuple of points, which are existing both trace1 and trace2
    get_matches_amount(trace1, trace2, in_percent, percent_from_first) - Function returns amount (numbers or percent) of
                                                                       points, which are existing both trace1 and trace2
"""

__author__ = 'Denis V. Gruzenkin'
__name__ = 'Analyse module'
__date__ = '09.11.2016'

from models.trace import Trace
from models.point import Point

def is_clones(trace1, trace2):
    """ Function for clones detecting

    :type trace1: Trace
    :param trace1: The 1-st trace for comparing
    :type trace2: Trace
    :param trace2:  The 2-nd trace for comparing
    :return: True - if traces are clones; False - otherwise
    """

    if trace1 == trace2:
        return True
    else:
        return False


def is_equal(trace1, trace2):
    """ Function for equal traces detecting

    :type trace1: Trace
    :param trace1: The 1-st trace for comparing
    :type trace2: Trace
    :param trace2: The 2-nd trace for comparing
    :return: True - if traces are equal; False - otherwise
    """

    if trace1.is_eq(trace2):
        return True
    else:
        return False


def get_matched_points(trace1, trace2):
    """ Function returns tuple of points, which are existing both trace1 and trace2

    :type trace1: Trace
    :param trace1: The 1-st trace for analysis
    :type trace2: Trace
    :param trace2: The 2-nd trace for analysis
    :return: tuple of Point instances
    """

    result_list = []
    for pnt1 in trace1.points_set:
        for pnt2 in trace2.points_set:
            if pnt1 == pnt2 and pnt1 not in result_list:
                result_list.append(pnt1)
    return tuple(result_list)


def get_matches_amount(trace1, trace2, in_percent = False, percent_from_first = True):
    """ Function returns amount (numbers or percent) of points, which are existing both trace1 and trace2

    :type trace1: Trace
    :param trace1: The 1-st trace for analysis
    :type trace2: Trace
    :param trace2: The 2-nd trace for analysis
    :type in_percent: bool
    :param in_percent: Flag for determine output measurement units: percents - if true, number - otherwise (by default)
    :type percent_from_first: bool
    :param percent_from_first: Flag for determine from which trace will calculate a percent. By default from the 1-st
    :return: amount (numbers or percent) of points
    """

    if in_percent is True and trace1 == trace2:
        return 100
    else:
        tmp_tpl = get_matched_points(trace1, trace2)
        if in_percent is False:
            return len(tmp_tpl)
        else:
            if percent_from_first:
                return len(tmp_tpl) / len(trace1.points_set) * 100
            else:
                return len(tmp_tpl) / len(trace2.points_set) * 100


def get_not_matched_points(trace1, trace2):
    """ Function returns tuple of points, which are existing only in trace1 or only in trace2

    :type trace1: Trace
    :param trace1: The 1-st trace for analysis
    :type trace2: Trace
    :param trace2: The 2-nd trace for analysis
    :return: tuple of Point instances
    """

    matched_points = get_matched_points(trace1, trace2)
    result_list = []
    for pnt in trace1.points_set + trace2.points_set:
        if pnt not in matched_points and pnt not in result_list:
            result_list.append(pnt)

    return tuple(result_list)


def get_not_matches_amount(trace1, trace2, in_percent = False, percent_from_first = True):
    """ Function returns amount of points, which are existing only in trace1 or only in trace2

    :type trace1: Trace
    :param trace1: The 1-st trace for analysis
    :type trace2: Trace
    :param trace2: The 2-nd trace for analysis
    :return: amount (numbers or percent) of points
    """

    return len(get_not_matched_points(trace1, trace2))


def similar_subtraces(trace1, trace2):
    """ This function realises Boyer-Moore search algorithm

    :param trace1:
    :param trace2:
    :return: if subsequence is found, then it returns subsequence tuple, otherwise - empty tuple
    """

    # 1 vector consists from 2 points and 1 edge. If these traces have only 1 common point, then there is no common way
    # between its
    if get_matches_amount(trace1, trace2) > 1:
        way, pattern = list(trace1.points_set), list(trace2.points_set)
        while len(pattern) > 1:
            if len(way) < len(pattern):
                way, pattern = pattern, way
            # Boyer-Moore search algorithm
            d = {i.position: len(pattern)for i in get_matched_points(trace1, trace2)+get_not_matched_points(trace1, trace2)}
            new_p = pattern[::-1]

            for i in range(len(new_p)):
                if d[new_p[i].position] != len(new_p):
                    continue
                else:
                    d[new_p[i].position] = i

            # x - start point of way
            # j - pattern counter
            # k - string counter
            len_p = x = j = k = len(pattern)
            # shifts counter
            counter = 0

            while x <= len(way) and j > 0:
                if pattern[j - 1] == way[k - 1]:
                    j -= 1
                    k -= 1
                else:
                    x += d[way[k - 1].position]
                    k = x
                    j = len_p
                    counter += 1
            if j <= 0:
                return tuple(pattern)
            else:
                pattern.pop(0)
    else:
        return ()

# TODO: V Для алгоритмов сортировки можно ввести метрику отношения длины массива (мерности точки) к количеству шагов
# TODO: V Написать функцию для нахождения общих отрезков пути у трасс!
# TODO: Для нахождения длин траекторий определить расстояния между сосендними точками http://edu.alnam.ru/book_math_al_3.php?id=56
# TODO: И между начальной и конечной
# TODO: Почитать про сталярное произведение векторов в N-мерном пространстве
# https://github.com/IvanYakimov/zond
# TODO: Изменить формат трасс во входном файле
# TODO: Написать 2-3 алгоритма сортировки на плоском C
