"""
"""

import unittest
from main import strings_to_traces, strings_2_traces
from models.point import Point
from models.trace import Trace

class MyMainTest(unittest.TestCase):
    """
    """

    def test_something(self):
        """
        :return:
        """

        input_file_name = "../input.txt"
        try:
            f = open(input_file_name, 'rt', -1, "utf-8")
        except IOError:
            print('Cannot open file ', input_file_name)
        else:
            input_data = f.readlines()
            f.close()

            # (-1.2,5.5,-3.7,4,2),(1,3,4,2,5),(1,3,2,4,5),(1,2,3,4,5)
            # (1.3,5,3,4,2),(1,5,3,4,2),(1,2,3,4,5)
            pnt11 = Point("Point1",(-1.2,5.5,-3.7,4,2))
            self.assertEqual(strings_to_traces(input_data), False)

    def test_strings_2_tracees(self):
        """

        :return:
        """
        input_file_name = "../input.txt"
        try:
            f = open(input_file_name, 'rt', -1, "utf-8")
        except IOError:
            print('Cannot open file ', input_file_name)
        else:
            input_data = f.readlines()
            f.close()

            pnt1 = Point('function1', (1, 2, 3))
            pnt2 = Point('function1', (2, 1, 3))
            pnt3 = Point('function2', (1, 3, 2))
            pnt4 = Point('function2', (3, 2, 1))
            pnt5 = Point('function2', (1, 2, 3))
            trc1 = Trace('function1', (pnt1, pnt2))
            trc2 = Trace('function2', (pnt3, pnt4, pnt5))
            traces_list = [trc1, trc2]

            self.assertEqual(strings_2_traces(input_data), traces_list)


if __name__ == '__main__':
    unittest.main()
