""" Module for my_libs.trace_analysis_lib module unit-testing

It is containing class MyTraceAnalysisTest, which is heritor of unittest.TestCase
"""

__author__ = 'Denis V. Gruzenkin'
__name__ = 'Tests for class Trace'
__date__ = '09.11.2016'

import unittest
from my_libs.trace_analysis_lib import is_clones, is_equal, get_matched_points, get_matches_amount\
    , get_not_matched_points, get_not_matches_amount, similar_subtraces
from models.trace import Trace
from models.point import Point


class MyTraceAnalysisTest(unittest.TestCase):
    """ MyTraceAnalysisTest class is containing test method for Trace analysis functions from my_libs.trace_analysis_lib

    Methods:
        * test_analysis_functions(self) - This method contains tests for every function from my_libs.trace_analysis_lib
    """

    def test_analysis_functions(self):
        """ This method contains tests for every function from my_libs.trace_analysis_lib

        :return: void
        """

        pnt11 = Point('TestPoint11', (9, 8, 7, 5, -5))
        pnt12 = Point('TestPoint12', (1, 2, 3, 4, -5))
        pnt21 = Point('TestPoint21', (9, 8, 6, 5, -5))
        pnt22 = Point('TestPoint22', (1, 2, 3, 4, 5.5))

        trc1 = Trace('TestTrace1', (pnt11, pnt12, pnt12))
        trc2 = Trace('TestTrace2', (pnt11, pnt12, pnt12))
        trc3 = Trace('TestTrace3', (pnt11, pnt22, pnt12))
        trc4 = Trace('TestTrace4', (pnt21, pnt22, pnt22))
        trc5 = Trace('TestTrace5', (pnt21, pnt22, pnt11))
        trc6 = Trace('TestTrace6', (pnt21, pnt22, pnt11, pnt12))

        # is_clones function testing
        self.assertEqual(is_clones(trc1, trc2), True)
        self.assertEqual(is_clones(trc1, trc3), False)

        # is_equal function testing
        self.assertEqual(is_equal(trc1, trc2), True)
        self.assertEqual(is_equal(trc1, trc3), True)
        self.assertEqual(is_equal(trc1, trc4), False)

        # get_matched_points function testing
        self.assertEqual(get_matched_points(trc1, trc2), (pnt11, pnt12))
        self.assertEqual(get_matched_points(trc1, trc3), (pnt11, pnt12))
        self.assertEqual(get_matched_points(trc3, trc4), (pnt22,))
        self.assertEqual(get_matched_points(trc1, trc4), ())

        # get_matches_amount function testing
        self.assertEqual(get_matches_amount(trc1, trc2), 2)  # Number value
        self.assertEqual(get_matches_amount(trc1, trc3), 2)  # Number value
        self.assertEqual(get_matches_amount(trc3, trc4), 1)  # Number value
        self.assertEqual(get_matches_amount(trc1, trc4), 0)  # Number value
        self.assertEqual(get_matches_amount(trc1, trc2, True), 100)  # Percent value from trc1
        self.assertEqual(get_matches_amount(trc3, trc4, True), 33.33333333333333)  # Percent value from trc3
        self.assertEqual(get_matches_amount(trc5, trc6, True), 100)  # Percent value from trc5
        self.assertEqual(get_matches_amount(trc6, trc5, True), 75)  # Percent value from trc6
        self.assertEqual(get_matches_amount(trc6, trc5, True, False), 100)  # Percent value from trc5

        # get_not_matched_points
        self.assertEqual(get_not_matched_points(trc1, trc2), ())
        self.assertEqual(get_not_matched_points(trc1, trc3), (pnt22,))
        self.assertEqual(get_not_matched_points(trc1, trc4), (pnt11, pnt12, pnt21, pnt22))

        # get_not_matches_amount
        self.assertEqual(get_not_matches_amount(trc1, trc2), 0)  # Number value
        self.assertEqual(get_not_matches_amount(trc1, trc3), 1)  # Number value
        self.assertEqual(get_not_matches_amount(trc1, trc4), 4)  # Number value

        # similar_subtraces
        self.assertEqual(similar_subtraces(trc5, trc6), (pnt21, pnt22, pnt11))
        self.assertEqual(similar_subtraces(trc1, trc4), ())

if __name__ == '__main__':
    unittest.main()
