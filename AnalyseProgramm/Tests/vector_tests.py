""" Module for models.vector module unit-testing

It is containing class MyVectorTest, which is heritor of unittest.TestCase
"""

import unittest
from models.vector import Vector
from models.point import Point
from math import acos, sqrt, pi


class MyVectorTest(unittest.TestCase):
    """

    """

    def test_init(self):
        """ Method for object initialization testing

        :return: void
        """

        pnt1 = Point('Point_start', (3, 1, 2))
        pnt2 = Point('Point_end', (1, 3, 2))
        pnt3 = Point()

        vector1 = Vector()
        vector2 = Vector(pnt1, pnt2)

        self.assertEqual(vector1.point_start, pnt3)
        self.assertEqual(vector1.point_end, pnt3)
        self.assertEqual(vector2.point_start, pnt1)
        self.assertEqual(vector2.point_end, pnt2)

    def test_get_scalar_value(self):
        """ Method for scalar value an a vector calculation testing

        :return: void
        """

        pnt1 = Point('Point1', (3, 1, 2))
        pnt2 = Point('Point2', (1, 3, 2))
        pnt3 = Point('Point3', (6, 5, 4))

        vector1 = Vector(pnt1, pnt3)
        vector2 = Vector(pnt1, pnt2)

        self.assertEqual(vector1.get_scalar_value(), (18, 5, 8))
        self.assertEqual(vector2.get_scalar_value(), (3, 3, 4))

    def test_get_scalar_product(self):
        """ Method for scalar product testing

        :return: void
        """

        pnt1 = Point('Point1', (3, 1, 2))
        pnt2 = Point('Point2', (1, 3, 2))
        pnt3 = Point('Point3', (6, 5, 4))

        vector1 = Vector(pnt1, pnt3)
        vector2 = Vector(pnt1, pnt2)

        self.assertEqual(vector1.get_scalar_product(), 31)
        self.assertEqual(vector2.get_scalar_product(), 10)

    def test_get_length(self):
        """ Method for length calculating function testing

        :return: void
        """

        pnt1 = Point('Point1', (3, 1, 2))
        pnt2 = Point('Point2', (1, 3, 2))
        pnt3 = Point('Point3', (6, 5, 4))
        pnt4 = Point('Point3', (-6, -5, 4))

        vector1 = Vector(pnt1, pnt3)
        vector2 = Vector(pnt1, pnt2)
        vector3 = Vector(pnt1, pnt4)

        self.assertEqual(vector1.get_length(), sqrt(31))
        self.assertEqual(vector2.get_length(), sqrt(10))
        self.assertEqual(vector3.get_length(), sqrt(15))

    def test_get_corner_between_pnts(self):
        """ Method for corner between 2 Points calculation testing

        :return: void
        """

        pnt1 = Point('Point1', (3, 1, 2))
        pnt2 = Point('Point2', (1, 3, 2))
        pnt3 = Point('Point3', (6, 5, 4))

        vector1 = Vector(pnt1, pnt3)
        vector2 = Vector(pnt1, pnt2)

        corn11 = vector1.get_corner_between_pnts()
        corn12 = acos(((31 / 9) * pi) / 180)
        corn21 = vector2.get_corner_between_pnts()
        corn22 = acos(((10 / 9) * pi) / 180)

        self.assertEqual(corn11, corn12)
        self.assertEqual(corn21, corn22)

    def test_get_corner_between_vectors(self):
        """ Method for corner between 2 Vectors calculation testing

        :return: void
        """

        pnt1 = Point('Point1', (3, 1, 2))
        pnt2 = Point('Point2', (1, 3, 2))
        pnt3 = Point('Point3', (6, 5, 4))
        pnt4 = Point('Point4', (5, 4, 3, 2))
        pnt5 = Point('Point5', (1, 2, 3, 4))

        vector1 = Vector(pnt1, pnt3)
        vector2 = Vector(pnt1, pnt2)
        vector3 = Vector(pnt4, pnt5)

        corn11 = vector1.get_corner_between_vectors(vector2)
        corn12 = acos(((101 / 9) * pi) / 180)
        corn21 = vector2.get_corner_between_vectors(vector3)
        corn22 = False

        self.assertEqual(corn11, corn12)
        self.assertEqual(corn21, corn22)

    def test_equivalents(self):
        """ Method for equivalents operations testing

        :return: void
        """

        pnt1 = Point('Point1', (3, 1, 2))
        pnt2 = Point('Point2', (1, 2, 3))
        pnt3 = Point('Point3', (6, 5, 4))
        pnt4 = Point('Point4', (5, 4, 3, 2))
        pnt5 = Point('Point5', (1, 2, 3, 4))

        vector1 = Vector(pnt1, pnt5)
        vector2 = Vector(pnt1, pnt2)
        vector3 = Vector(pnt4, pnt5)
        vector4 = Vector(pnt3, pnt4)
        vector5 = Vector(pnt3, pnt4)

        self.assertEqual(vector4 == vector5, True)
        self.assertEqual(vector3 == vector5, False)
        self.assertEqual(vector1 == vector2, False)
        self.assertEqual(vector1 != vector2, True)
        self.assertEqual(vector4 != vector5, False)

if __name__ == '__main__':
    unittest.main()
