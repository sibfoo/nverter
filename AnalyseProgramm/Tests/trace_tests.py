""" Module for models.trace module unit-testing

It is containing class MyTraceTest, which is heritor of unittest.TestCase
"""

__author__ = 'Denis V. Gruzenkin'
__name__ = 'Tests for class Trace'
__date__ = '09.11.2016'

import unittest
from models.trace import Trace
from models.point import Point
from models.vector import Vector
from math import sqrt


class MyTraceTest(unittest.TestCase):
    """ MyTraceTest class is containing several test methods for Trace class from models.trace module

    Methods:
        * test_init(self) - Method for object initialization testing
        * test_equal(self) - Method for Trace equal operators testing
    """

    def test_init(self):
        """ Method for object initialization testing

        :return: void
        """

        trc1 = Trace()
        self.assertEqual(trc1.name, 'noSetName')
        self.assertEqual(trc1.points_set, ())
        self.assertEqual(len(trc1.points_set), 0)

        pnt1 = Point()
        pnt2 = Point('TestPoint', (1, 2, 3, 4, -5))
        trc2 = Trace('TestTrace', (pnt1, pnt2))
        self.assertEqual(trc2.name, 'TestTrace')
        self.assertEqual(trc2.points_set, (pnt1, pnt2))

        pnt3 = Point('Point3', (-5, 1, 2, 3, 4))
        pnt4 = Point('Point4', (1, 2, -5, 3, 4))
        trc3 = Trace('TestTrace2', (pnt2, pnt3, pnt4))
        forward_vector = Vector(pnt2, pnt4)
        vector1 = Vector(pnt2, pnt3)
        vector2 = Vector(pnt3, pnt4)
        self.assertEqual(trc3.forward_way, forward_vector)
        self.assertEqual(trc3.edges, [vector1, vector2])

    def test_equal(self):
        """ Method for Trace equal operators testing

        :return: void
        """

        pnt11 = Point('TestPoint11', (9, 8, 7, 5, -5))
        pnt12 = Point('TestPoint12', (1, 2, 3, 4, -5))
        trc1 = Trace('TestTrace1', (pnt11, pnt12))

        pnt21 = Point('TestPoint21', (9, 8, 7, 5, -5))
        pnt22 = Point('TestPoint22', (1, 2, 3, 4, -5))
        trc2 = Trace('TestTrace2', (pnt21, pnt22))

        pnt31 = Point('TestPoint31', (9, 8, 6, 5, -5))
        pnt32 = Point('TestPoint32', (1, 2, 3, 4, 5.5))
        trc3 = Trace('TestTrace3', (pnt31, pnt32))

        self.assertEqual(trc1 == trc2, True)
        self.assertEqual(trc1 != trc2, False)
        self.assertEqual(trc2 == trc3, False)
        self.assertEqual(trc2 != trc3, True)

        trc4 = Trace('TestTrace4', (pnt11, pnt12, pnt12))
        trc5 = Trace('TestTrace5', (pnt11, pnt12, pnt12))
        trc6 = Trace('TestTrace6', (pnt11, pnt32, pnt12))
        trc7 = Trace('TestTrace7', (pnt31, pnt32, pnt12))

        self.assertEqual(trc4.is_eq(trc5), True)
        self.assertEqual(trc4 == trc5, True)
        self.assertEqual(trc4.is_eq(trc6), True)
        self.assertEqual(trc4 != trc6, True)
        self.assertEqual(trc4.is_eq(trc7), False)
        self.assertEqual(trc4 != trc7, True)

    def test_ratio_dimension_length(self):
        """ Method for ratio dimension length calculation testing

        :return: void
        """

        pnt11 = Point('TestPoint11', (9, 8, 7, 6, 5))
        pnt12 = Point('TestPoint12', (0, 1, 2, 3, 4))
        pnt21 = Point('TestPoint21', (0, -1, -2, -3, -4))
        pnt22 = Point('TestPoint22', (-9, -8, -7, -6, -5))

        trc1 = Trace('TestTrace1', (pnt11, pnt12))
        trc2 = Trace('TestTrace3', (pnt11, pnt12, pnt21, pnt22, pnt11))
        trc3 = Trace('TestTrace3', (pnt11, pnt12, pnt21, pnt22, pnt11, pnt12))

        self.assertEqual(trc1.ratio_dimension_length(), 2.5)
        self.assertEqual(trc2.ratio_dimension_length(), 1)
        self.assertEqual(trc3.ratio_dimension_length(), 5/6)

    def test_get_forward_way_length(self):
        """ Method for length of forward way calculation testing

        :return: void
        """

        pnt1 = Point('Point1', (-4, 2, 3, 4, 5))
        pnt2 = Point('Point2', (1, 2, 3, 4, -5))
        pnt3 = Point('Point3', (-5, 1, 2, 3, 4))
        pnt4 = Point('Point4', (1, 2, -5, 3, 4))
        trc1 = Trace('TestTrace1', (pnt1, pnt3, pnt2))
        trc2 = Trace('TestTrace2', (pnt2, pnt3, pnt4))
        self.assertEqual(trc1.get_forward_way_length(), 0)
        self.assertEqual(trc2.get_forward_way_length(), sqrt(18))

    def test_get_trace_length(self):
        """ Method for length of all way calculation testing

        :return: void
        """

        pnt1 = Point('Point1', (-4, 2, 3, 4, 5))
        pnt2 = Point('Point2', (1, 2, 3, 4, -5))
        pnt3 = Point('Point3', (-5, 1, 2, 3, 4))
        pnt4 = Point('Point4', (1, 2, -5, 3, 4))
        trc1 = Trace('TestTrace1', (pnt1, pnt3, pnt2))
        trc2 = Trace('TestTrace2', (pnt2, pnt3, pnt4))
        self.assertEqual(trc1.get_trace_length(), trc1.edges[0].get_length() + trc1.edges[1].get_length())
        self.assertEqual(trc2.get_trace_length(), trc2.edges[0].get_length() + trc2.edges[1].get_length())

if __name__ == '__main__':
    unittest.main()
