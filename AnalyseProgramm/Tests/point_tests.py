""" Module for models.point module unit-testing

It is containing class MyPointTest, which is heritor of unittest.TestCase
"""

__author__ = 'Denis V. Gruzenkin'
__name__ = 'Tests for class Point'
__date__ = '07.11.2016'

import unittest
from models.point import Point


class MyPointTest(unittest.TestCase):
    """ MyPointTest class is containing several test methods for Point class from models.point module

    Methods:
        * test_init(self)
        * test_operators(self)
    """

    def test_init(self):
        """ Method for object initialization testing

        :return: void
        """

        pnt1 = Point()
        self.assertEqual(pnt1.name, 'noName')
        self.assertEqual(len(pnt1.position), 0)

        pnt2 = Point('MyPoint', (1,2,3,4,-5))
        self.assertEqual(pnt2.name, 'MyPoint')
        self.assertEqual(pnt2.position, (1,2,3,4,-5))

    def test_operators(self):
        """ Method for Point binary operators testing

        :return: void
        """

        pnt1 = Point('Point1', (2, 3, 4))
        pnt2 = Point('Point2', (5, 7, 9))
        pnt3 = Point('Point3', (5, 7, 9, 5))
        pnt4 = Point('Point4', (5, 7, 9, 5))

        pnt_res1 = Point('Point2&Point1', (7,10,13))
        pnt_res2 = Point('Point2&Point1', (3,4,5))
        pnt_res3 = Point('Point2&Point1', (10,21,36))
        pnt_res4 = Point('Point2&Point1', (2.5,2.3333333333333335,2.25))

        self.assertEqual(pnt2 + pnt1, pnt_res1)
        with self.assertRaises(NameError):
            pnt3 + pnt1
        self.assertEqual(pnt2 - pnt1, pnt_res2)
        with self.assertRaises(NameError):
            pnt3 - pnt1
        self.assertEqual(pnt2 * pnt1, pnt_res3)
        with self.assertRaises(NameError):
            pnt3 * pnt1
        self.assertEqual(pnt2 / pnt1, pnt_res4)
        with self.assertRaises(NameError):
            pnt3 / pnt1

        self.assertEqual(pnt3 == pnt4, True)
        self.assertEqual(pnt3 == pnt2, False)
        self.assertEqual(pnt1 == pnt2, False)

if __name__ == '__main__':
    """ Point of testing module entry """
    unittest.main()
