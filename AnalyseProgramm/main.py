""" N-Versions trace analyse program

It is a program for N-versions algorithm level diversification determination by their some trace analysing
"""

import getopt
import re
import sys
import os
from my_libs.trace_analysis_lib import is_clones, is_equal, get_matched_points, get_matches_amount, similar_subtraces\
    , get_not_matched_points, get_not_matches_amount
from ast import literal_eval
from models.point import Point
from models.trace import Trace
from models.vector import Vector
from math import sqrt

__author__ = 'Denis V. Gruzenkin'
__name__ = 'NVerTer'
__date__ = '07.11.2016'


def strings_2_traces(strings_set):
    """ Convert string set to list of traces

    :type strings_set: str
    :param strings_set: raw strings from input text file
    :return: list of traces or ValueError
    """

    if strings_set is not None and strings_set != '':
        # traces_list = []

        # File format:
        # function1:&{1, 2, ..., n}->SomeValue
        # function1:&{2, 1, ..., n}->SomeValue
        # function2:&{1, 3, ..., n}->SomeValue
        # ...
        input_str_pattern = re.compile('[\w\-\. ]+:{1}&{1}\{{1}[0-9., -]+\}{1}.*\s')
        points_tuple = []
        for my_str in strings_set:
            matched = input_str_pattern.match(my_str)  # Check input string format
            if matched:  # If format is correct,
                try:
                    matched_name = re.search('[\w\-\. ]+', my_str).group(0)
                    matched_arr = literal_eval(re.search('\{{1}[0-9., -]+\}{1}', my_str).group(0).replace(' ', '').replace('{', '(').replace('}', ')'))
                except AttributeError:
                    matched_name = ''
                    matched_arr = ''
                if matched_arr != '' and matched_name != '':
                    tmp_point = Point(matched_name, matched_arr)
                    if len(points_tuple) == 0: points_tuple.append(tmp_point)
                    # Имя сравнивается для того, чтобы была вохможность записи разных трасс с одинаковыми точками
                    elif tmp_point.position != points_tuple[len(points_tuple) - 1].position or \
                                    tmp_point.name != points_tuple[len(points_tuple) - 1].name:
                        points_tuple.append(tmp_point)
                    # print(matched_name)
                    # print(tuple(literal_eval(matched_arr)))
                else:
                    print('There is no data in valid string! It is really strange...')
            else:
                print('Incorrect format! String: "' + my_str + '" is not matches with template')
        try:
            print('Try opening/creating output file')
            f_ = open('output_points', 'wt', -1, "utf-8")
        except IOError:
            print('Cannot open file ')
        else:
            #print(f_.writelines(points_tuple))
            if f_.writelines(str(points_tuple)): # TODO: Сделать нормалный вывод точек в файл и посмотреть, отфильтровывает ли их прога!
                print("Results are successfully wtiten")
            else:
                print("Output file writing error")
            f_.close()
        my_traces_list = []
        trace_points = []
        for i in range(0, len(points_tuple)):
            if i == 0:
                trace_points.append(points_tuple[i])
            else:
                if trace_points[len(trace_points) - 1].name == points_tuple[i].name:
                    trace_points.append(points_tuple[i])
                else:
                    my_traces_list.append(Trace(trace_points[len(trace_points) - 1].name, tuple(trace_points)))
                    trace_points.clear()
                    trace_points.append(points_tuple[i])
        # Создаём последнюю трассу, котору не получилось создать в цикле, т.к. в else в последний раз не проваливаемся
        my_traces_list.append(Trace(trace_points[len(trace_points) - 1].name, tuple(trace_points)))

        return my_traces_list
    else:
        raise ValueError('Incorrect value. String is none or empty!')


def main():
    """ Point of program entry

    :return: void
    """
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:o:c:", ["help", "input=", "output=", "convert="])
    except getopt.GetoptError as err:
        # print help information and exit:
        print(err)  # will print something like "option -a not recognized"
        sys.exit(2)
    else:
        is_needed_calculation = True
        input_file_name = "input.txt"
        output_file_name = "output.txt"
        for o, a in opts:
            if o in ("-o", "--output"):
                output_file_name = a
            elif o in ("-i", "--input"):
                input_file_name = a
            elif o in ("-h", "--help"):
                print("-h or --help is used to print command help\n-i or --input is used to enter input file name\n"
                      "-o or --output is used to enter output file name\n-c or --convert is used to convert raw data "
                      "file into correct input file")
                sys.exit()
            elif o in ("-c", "--convert"):
                is_needed_calculation = False
                try:
                    converted_file = open(a)
                except IOError:
                    print('Cannot open file ', a)
                else:
                    tmp_data = converted_file.readlines()

                    # Input converted file format:
                    # #frame 0:  #frame 1: {6,6,-8,6}  #frame 2: 0 0 0 4
                    # #frame 0:  #frame 1: {6,6,-8,6}  #frame 2: 0 0 0 4
                    # #frame 0:  #frame 1: {6,6,-8,6}  #frame 2: 0 0 0 4
                    input_str_pattern = re.compile('[\w\s#:]+\{{1}[0-9., -]+\}{1}\s{1}\n')
                    input_str_pattern2 = re.compile('[\w\s#:]+\{{1}[0-9., -]+\}{1}[\w\s#:]+')
                    conversion_result = []

                    counter = 1
                    for my_str in tmp_data:
                        matched = input_str_pattern.match(my_str)  # Check input string format
                        matched2 = input_str_pattern2.match(my_str)
                        if matched or matched2:  # If format is correct,
                            try:
                                p, only_file_name = os.path.split(converted_file.name)
                                conv_str = re.sub('[\w\s#:]+\{', only_file_name + '_' + str(counter) + ':&{', my_str)
                                conversion_result.append(conv_str)

                                if conv_str.find('#frame 2') == -1:
                                    counter += 1

                            except AttributeError:
                                print('There is no data in valid string! It is really strange...')
                        else:
                            print('Incorrect format! String: "' + my_str + '" is not matches with template')

                    converted_file.close()

                    try:
                        new_result_file = open('new_result.txt', 'wt', -1, "utf-8")
                    except IOError:
                        print('Cannot open file ', output_file_name)
                    else:
                        if new_result_file.writelines(conversion_result):
                            print("Output file writing error")
                        else:
                            print("Results are successfully writen")
                        new_result_file.close()
            else:
                assert False, "Unhandled option"

        if is_needed_calculation:
            try:
                print('Try opening input file')
                f = open(input_file_name, 'rt', -1, "utf-8")
            except IOError:
                print('Cannot open file ', input_file_name)
            else:
                print('File is opened')
                input_data = f.readlines()
                f.close()
                traces = strings_2_traces(input_data)
                print('Data was read')

                print('Calculation process is started')
                # TODO: Сделать обработку исключений здесь обязательно!!!
                f_graph = open('Graphic_traces.csv', 'wt', -1, "utf-8")
                output_result = []
                for cur_trace in traces:
                    trace_title = '\nTrace "' + cur_trace.name + '"\n'
                    output_result.append(trace_title)
                    # print(trace_title)

                    forward_way = cur_trace.get_forward_way_length()
                    test_forward_way = "Forward way: " + str(forward_way) + '\n'
                    output_result.append(test_forward_way)
                    # print(test_forward_way)

                    trace_length = cur_trace.get_trace_length()
                    test_trace_length = "Trace length: " + str(trace_length) + '\n'
                    output_result.append(test_trace_length)
                    # print(test_trace_length)

                    ratio_dimension_length_res = cur_trace.ratio_dimension_length()
                    ratio_dimension_length_str = 'The ration of space dimensions to trace length is ' \
                                                 + str(ratio_dimension_length_res) + '\n';
                    output_result.append(ratio_dimension_length_str)

                    for tmp_trace in traces:
                        if is_clones(cur_trace, tmp_trace):
                            is_clones_result = 'Traces ' + str(cur_trace.name) + ' and ' + str(tmp_trace.name) \
                                               + ' are clones\n'
                        output_result.append(is_clones_result)

                        if is_equal(cur_trace, tmp_trace):
                            is_equal_result = 'Traces ' + str(cur_trace.name) + ' and ' + str(tmp_trace.name) \
                                              + ' are equal\n'
                        output_result.append(is_equal_result)

                        matched_points_tpl = get_matched_points(cur_trace, tmp_trace)
                        matched_points_str = ''
                        for mp in matched_points_tpl:
                            matched_points_str += str(mp)
                        if len(matched_points_tpl) > 0:
                            result_matched_points_tpl = 'Traces ' + str(cur_trace.name) + ' and ' + str(tmp_trace.name) \
                                                        + ' have identical points: ' + matched_points_str + '\n'
                        output_result.append(result_matched_points_tpl)
                        output_result.append('Identical points number: ' + str(len(matched_points_str)) + '\n')

                        subsequence = similar_subtraces(cur_trace, tmp_trace)
                        subsequence_str = ''
                        if subsequence is not None and len(subsequence) > 0:
                            subsequence_str = 'Traces have equivalent subtrace(s): ' + str(subsequence) + '\n'
                        output_result.append(subsequence_str)

                        not_matched_points = get_not_matches_amount(cur_trace, tmp_trace)
                        if not_matched_points > 0:
                            output_result.append('Not identical points number: ' + str(not_matched_points) + '\n')
                            output_result.append('Identical points number / Not identical points number: '
                                                 + str(len(matched_points_tpl) / not_matched_points) + '\n')

                    # Записываем значения в файл для последующего построения по ним графика
                    finale_vector = Vector(cur_trace.points_set[len(cur_trace.points_set) - 1],
                                           cur_trace.points_set[len(cur_trace.points_set) - 1])
                    finale_len = sqrt(finale_vector.get_scalar_product())
                    f_graph.write(cur_trace.name + ';')
                    for current_point in cur_trace.points_set:
                        tmp_vector = Vector(current_point, cur_trace.points_set[len(cur_trace.points_set) - 1])
                        cur_len = sqrt(abs(tmp_vector.get_scalar_product()))
                        f_graph.write(str(finale_len - cur_len) + ';')
                    f_graph.write('\n')

                f_graph.close()

                # print(output_result)
                print('Calculation process is ended')

                try:
                    print('Try opening/creating output file')
                    f_ = open(output_file_name, 'wt', -1, "utf-8")
                except IOError:
                    print('Cannot open file ', output_file_name)
                else:
                    print(f_.writelines(output_result))
                    if f_.writelines(output_result):
                        print("Results are successfully wtiten")
                    else:
                        print("Output file writing error")
                    f_.close()

main()  # Не знаю, почему не работает условие ниже, но программа запускается пока только так

if __name__ == "__main__":
    sys.exit(main())
